function varargout = new(varargin)
% NEW MATLAB code for new.fig
%      NEW, by itself, creates a new NEW or raises the existing
%      singleton*.
%
%      H = NEW returns the handle to a new NEW or the handle to
%      the existing singleton*.
%
%      NEW('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NEW.M with the given input arguments.
%
%      NEW('Property','Value',...) creates a new NEW or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before new_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to new_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES
% Edit the above text to modify the response to help new

% Last Modified by GUIDE v2.5 26-Oct-2014 23:04:33
% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @new_OpeningFcn, ...
                   'gui_OutputFcn',  @new_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- before new is made visible.
function new_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to new (see VARARGIN)

clearvars -GLOBAL interface dataS structIn
global structIn dataS interface
structIn                        = evalin('base','struct');
dataS							= init(structIn);

% Choose default command line output for new
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);
refresh_list();
dessine();
interface.sel.descripteurs	= 1;
% UIWAIT makes new wait for user response (see UIRESUME)
%uiwait(handles.figure1);

% --- initialise le dataset et l'interface
function out = init(struct)

global interface
nuance  = [];
piece   = [];
pianiste= [];
nomatch = [];
obs		= [];

%initialise le dataset
for i = 1:length(struct)
        obs = [obs;struct(i).obs];
        nuance      = [nuance;{struct(i).nuance}];
        piece       = [piece;struct(i).piece];
        pianiste    = [pianiste;struct(i).pianiste];
		nomatch		= [nomatch;struct(i).noMATCH];
	end
	
nuance      = nominal(nuance);
pianiste    = nominal(pianiste);
out			= dataset(obs,nuance,piece,pianiste,nomatch);

%on fixe les temps arbitrairement
interface.sel.Pdebut	= 1;
interface.sel.Tdebut	= 0;
%--
interface.sel.Pfin		= 1;
interface.sel.Tfin		= 0;

interface.sel.pannel = 1;
interface = get_interface_values;

% --- Outputs from this function are returned to the command line.
function varargout = new_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% Get default command line output from handles structure
varargout{1} = handles.output;

% =========================================================================
% =========================================================================

% --- Play.
function Play_Callback(hObject, eventdata, handles)

global structIn player
interface = get_interface_values;
debfin = tableau_accords(interface.sel.obs);

if isfield(interface.sel,'Pdebut') && isfield(interface.sel,'Pfin')
dataOut = get_dataSet();
%dataOut = cat_temps(dataOut)

%% Audio
%[y,Fs]=wavread(structIn(interface.sel.obs).chemin);

y=get(miraudio(structIn(interface.sel.obs).chemin,'TrimStart'),'Data');
y = y{1}{1};
Fs = 44100;
N = length(y);
slength = N/Fs;
t = linspace(0, N/Fs, N);
%% on cr�e le graphe
titre = [interface.sel.pianiste,' piece : ',num2str(interface.sel.piece),...
	' observation : ',num2str(interface.sel.obs),...
	' nuance : ',interface.sel.nuance];
fig = figure; hold on;
set(fig,'Position',[1 1 1310 505],'MenuBar','none','Name',titre,'NumberTitle','off');
plot(t,y, 'b'); % plot audio data
title(titre);
xlabel(strcat('Temps en Sec (fs = ', num2str(Fs), ')'));
ylabel('Amplitude');
ylimits = get(gca, 'YLim'); % get the y-axis limits
plotdata = [ylimits(1):0.1:ylimits(2)];
hline = plot(repmat(0, size(plotdata)), plotdata, 'y'); % plot the marker
 for i=1:length(debfin)
line([debfin(i,1) debfin(i,1)],[ylimits(1) ylimits(2)],...
	'Marker','.','LineStyle','--','color','g');
line([debfin(i,2) debfin(i,2)],[ylimits(1) ylimits(2)],...
	'Marker','.','LineStyle','-','color','r');
end
%% instantiate the audioplayer object
player = audioplayer(y, Fs);

%% setup the timer for the audioplayer object
player.TimerFcn = {@plotMarker, player, fig, plotdata}; % timer callback function (defined below)
player.TimerPeriod = 0.01; % period of the timer in seconds

%% start playing the audio
% this will move the marker over the audio plot at intervals of 0.01 s
play(player);



% a = miraudio(structIn(interface.sel.obs).chemin,'Extract',...
% 	interface.sel.Tdebut,...
% 	interface.sel.Tfin,...
% 	's','Sampling',48000)
% mirplay(a);

end

% --- save.
function save_Callback(hObject, eventdata, handles)

interface = get_interface_values;
dataOut = get_dataSet();
%dataOut = cat_temps(dataOut);


structOut = descripteurs(dataOut);


%affichage/sortie
disp(get(findobj('Tag','nom_data'),'String'));
nom_out = get(findobj('Tag','nom_data'),'String');
assignin('base', nom_out, structOut')

% --- pianiste.
function pianiste_SelectionChangeFcn(hObject, eventdata, handles)
refresh_list();
dessine;

% --- nuance.
function nuance_SelectionChangeFcn(hObject, eventdata, handles)
refresh_list();
dessine;

% --- piece.
function piece_SelectionChangeFcn(hObject, eventdata, handles)
refresh_list(); 
dessine;

% --- listbox1.
function listbox1_Callback(hObject, eventdata, handles)
dessine();

% --- repeuple la liste.
function refresh_list()


interface = get_interface_values;
dataOut = get_dataSet();
set(findobj('Tag','listbox1'),'String',strcat(num2str(dataOut.obs),'-',cellstr(dataOut.pianiste),'-',cellstr(dataOut.nuance))','Value',1);

if isfield(interface,'limites')
dataOut = horzcat(dataOut,temps(dataOut));
end

% --- dessine.
function dessine()

global structIn dataS interface

interface = get_interface_values;
set(findobj('Tag','nomatch'),'String',structIn(interface.sel.obs).noMATCH);

%on va chercher le match
interface.m = structIn(interface.sel.obs).match;
%ainsi que le tableau des accords.
debfin= tableau_accords(interface.sel.obs);

interface.w.notaAxes1 = findobj('Tag','notaAxes1');
interface.w.notaAxes2 = findobj('Tag','notaAxes2');
interface.w.perfAxes1 = findobj('Tag','perfAxes1');
interface.w.perfAxes2 = findobj('Tag','perfAxes2');



axes(interface.w.notaAxes1); interface.w.Pn=proll(interface.m.Nn,'sec','');

interface.w.notaAxes1Xlimlim = get(interface.w.notaAxes1, 'XLim');
ylimlim = [20 110];
interface.w.notaAxes1Ylimlim = ylimlim;
set(interface.w.notaAxes1, 'YLim', ylimlim);

xlim1 = get(interface.w.notaAxes1, 'XLim');
xlim2 = xlim1*gettempo(interface.m.Nn)/60;
set(interface.w.notaAxes2, 'XLim', xlim2, 'XGrid', 'on');
set(interface.w.notaAxes2, 'Ylim', ylimlim);
  
% ---

axes(interface.w.perfAxes1); interface.w.Pp = proll(interface.m.Np,'sec', '');
set(interface.w.perfAxes1, 'XLim', scalePerf(interface.w.notaAxes1Xlimlim));
interface.w.perfAxes1Xlimlim = get(interface.w.perfAxes1, 'XLim');
interface.w.perfAxes1Ylimlim = ylimlim;
set(interface.w.perfAxes1, 'YLim', ylimlim);

set(interface.w.perfAxes1, 'XLim', scalePerf(xlim1));
set(interface.w.perfAxes2, 'XLim', get(interface.w.perfAxes1, 'XLim'), 'XGrid', 'on');
t1 = get(interface.w.notaAxes2, 'XTick');
tlabel = get(interface.w.notaAxes2, 'XTickLabel');
[t2 p2] = scaleTicks(t1);
set(interface.w.perfAxes2, 'XTick', t2, 'XTickLabel', tlabel);
set(interface.w.perfAxes2, 'Ylim', ylimlim);

lightUp(1);
interface.sel.Pdebut	=	1;

set(findobj('Tag','figure1'), 'SelectionType','alt');
temp = interface.m.GIn(end,:);
temp(temp==0)=[];
lightUp(temp(end));
interface.sel.Pfin		=	length(interface.w.Pp);
set(findobj('Tag','figure1'), 'SelectionType','normal');

% --- renvoie les valeurs d'interface
function out		= get_interface_values

global interface
%on recup�re les champs de l' interface
out.sel.pianiste =...
	get(get(findobj('Tag','pianiste'),'SelectedObject'),'String');

out.sel.piece=...
	lower(get(get(findobj('Tag','piece'),'SelectedObject'),'String'));
out.sel.piece = strread(out.sel.piece,'%s','delimiter',' ');
out.sel.piece = str2num(out.sel.piece{2});

out.sel.nuance=...
lower(get(get(findobj('Tag','nuance'),'SelectedObject'),'String'));
out.sel.nuance = strrep(out.sel.nuance,'�','e');

%on recupere l'item selectionn� en ce moment
liste = get(findobj('Tag','listbox1'),'String');
liste = liste{get(findobj('Tag','listbox1'),'Value')};
	%on en split le nom
	liste = strsplit(liste,'-');
	liste = str2double(liste{1});
	out.sel.obs = liste;

%on recup�re les temps de d�but et de fin de selection (si ils existent)

out.sel.Pdebut = interface.sel.Pdebut;
out.sel.Tdebut	= interface.sel.Tdebut;
%--
out.sel.Pfin = interface.sel.Pfin;
out.sel.Tfin	= interface.sel.Tfin;

out.sel.pannel = interface.sel.pannel;

% --- concatene les temps des notes selectionnees au dataset(useless ?)
function out		= cat_temps(dataIn)

global structIn

interface	= get_interface_values;
Tdeb		=[];
Tfin		=[];

for i=1:length(dataIn)
	%si le match a bien �t� fait !!
    if(isfield(structIn(dataIn{i,1}),'match'))
    
		match = structIn(dataIn{i,1}).match;
		Id = interface.sel.Pdebut;
		%On cherche l'indice de la premi�re note de l'accord selectionn�
		[cc tmp]	= find(match.realGIp == Id);
		Id			= match.realGIp(cc,1);
		%On cherche � quelle ligne est l'indice dans le rapport de match
		tmp			= find(cell2mat(match.M(:,3)) == Id);
		%On regarde l'onset � la ligne qui correspond
		Tdeb		= [Tdeb;cell2mat(match.M(tmp,7))];
	%--
		If			= interface.sel.Pfin;
		%On cherche l'indice de la derni�re note de l'accord selectionn�
		[cc tmp]	= find(match.realGIp == If);
		temp2		= match.realGIp(cc,:);
		temp2(temp2==0)=[];
		If			=temp2(end);
		%On cherche � quelle ligne est l'indice dans le rapport de match
		tmp			= find(cell2mat(match.M(:,3)) == If);
		%On regarde l'onset � la ligne qui correspond et on y ajoute la
		%duree
		Tfin		= [Tfin;cell2mat(match.M(tmp,7)) + cell2mat(match.M(tmp,11))];
		
	%si le match n'a pas �t� fait
	else
	end
	
end
out = horzcat(dataIn,dataset(Tdeb,Tfin));

% --- recalcule le dataset de sortie
function out		= get_dataSet()

global dataS

interface = get_interface_values;

%on compose un dataset en fonction des champs
if(strcmp(interface.sel.pianiste,'Tous')~=1 && ...
        strcmp(interface.sel.nuance,'tous')~=1 )
   out = ...
   dataS(dataS.nuance==interface.sel.nuance & ...
   dataS.pianiste==interface.sel.pianiste & ...
   dataS.piece==interface.sel.piece ,:);

elseif(strcmp(interface.sel.nuance,'tous')==0 && strcmp(interface.sel.pianiste,'Tous')==1 )
        out = ...
   dataS(dataS.nuance==interface.sel.nuance & ...
   dataS.piece==interface.sel.piece ,:);

elseif(strcmp(interface.sel.nuance,'tous')==1 && strcmp(interface.sel.pianiste,'Tous')==0)
        out = ...
   dataS(dataS.pianiste==interface.sel.pianiste& ...
   dataS.piece==interface.sel.piece ,:);

elseif(strcmp(interface.sel.nuance,'tous')==1 && strcmp(interface.sel.pianiste,'Tous')==1)
        out = ...
   dataS(dataS.piece==interface.sel.piece ,:);
end
  
% --- scale
function [TP, PP]	= scaleTicks(beats)

global interface

TP = [];
PP = [];
for bb = 1:length(beats);
   pp = find(interface.m.Bp == beats(bb));
   
   if ~isempty(pp)
      pp = pp;
      tp = interface.m.Tp(pp);
   else
      pp1 = find(interface.m.Bp < beats(bb));
      pp2 = find(interface.m.Bp > beats(bb));
      
      if ~isempty(pp2)
	 pp2 = pp2(1);	 
      else
	 pp2 = length(interface.m.Bp);
	 pp1 = pp2-1;
      end
      if ~isempty(pp1)
	 pp1 = pp1(length(pp1));
      else
	 pp1 = 1;
         pp2 = 2;
      end

      db = diff(interface.m.Bp([pp1 pp2]));
      dt = diff(interface.m.Tp([pp1 pp2]));
      
      delta = beats(bb) - interface.m.Bp(pp1);
      
      pp = pp1;
      tp = interface.m.Tp(pp1) + delta * (dt/db);
   end

   TP = [TP; tp];
   PP = [PP; pp];
end
  
% --- scale
function xlimP		= scalePerf(xlimN)

global structIn

interface = get_interface_values;

nleft = find(structIn(interface.sel.obs).match.Tn <= xlimN(1));
nrght = find(structIn(interface.sel.obs).match.Tn >= xlimN(2));
if isempty(nrght); nrght = length(structIn(interface.sel.obs).match.Tn); end;
if isempty(nleft); nleft = 1; end;

nleft = nleft(length(nleft));
nrght = nrght(1);

ptl = structIn(interface.sel.obs).match.Tp(nleft);
ptr = structIn(interface.sel.obs).match.Tp(nrght);

ntl = structIn(interface.sel.obs).match.Tn(nleft);
ntr = structIn(interface.sel.obs).match.Tn(nrght);
if ntl == 0; ntl = 1; end;

xlimP = [xlimN(1)*(ptl/ntl) xlimN(2)*(ptr/ntr)];

% --- DescripteurButton.
function DescripteurButton_Callback(hObject, eventdata, handles)
global interface
[Selection,ok] = listdlg('ListString',{...
    'MirBrightness',...
    'MirKurtosis',...
    'MirAttackTime' ,...
    'MirFlatness' ,...
    'MirCentroid' ,...
    'MirSpread' ,...
    'MirEntropy' ,...
    'MirAttackLeap' ,...
    'MirSkewness', ...
	'MirAttackslope',...
	'Mirzerocross',...
	'Mirrollof',...
	'MirRMS'});
if ok==1
    interface.sel.descripteurs = Selection;
end

function structOut = descripteurs(dataOut)
global interface structIn

	%on r�cup�re les fichiers s�l�ctionn�s
	listbox = findobj('Tag','listbox1');
	%on r�cup�re les fichiers dans la listbox
	liste = get(listbox,'String');
	
	t_obs =[];
	%pour tous les fichiers pr�sents dans la liste
	for i=1:length(liste)
		temp = strsplit(liste{i},'-');
		t_obs = str2double(temp{1});

		%on va chercher les temps de debut/fin de chaque accord
		structIn(t_obs).debfin ...
			= tableau_accords(t_obs);

		%On calcule les descripteurs pour chaque debut/fin
		structOut(i) = calcul_descripteurs(t_obs);
	end
	
function structOut = calcul_descripteurs(t_obs)

global structIn interface 

temps = structIn(t_obs).debfin;
structOut = structIn(t_obs);
disp(t_obs)
%	  'MirBrightness',...
%     'MirKurtosis',...
%     'MirAttackTime' ,...
%     'MirFlatness' ,...
%     'MirCentroid' ,...
%     'MirSpread' ,...
%     'MirEntropy' ,...
%     'MirAttackLeap' ,...
%     'MirSkewness', ...
% 	'MirAttackslope',...
% 	'Mirzerocross',...
% 	'Mirrollof'
%	'MirRMS});
bright =[];
kurt =[];
attackt =[];
flat =[];
centroid =[];
spread =[];
entrop =[];
attleap =[];
skew =[];
attslp =[];
zerocross =[];
rolloff =[];
RMS = [];
%on switche sur les descripteurs selectionn�s

for ii=1:length(structIn(t_obs).debfin)
				a = miraudio(structIn(t_obs).chemin,...
					'Extract',temps(ii,1),temps(ii,2),'s');
	
		if(sum(interface.sel.descripteurs==1))
					
				bright = [bright;mirgetdata(mirbrightness(a))];
			
			structOut.bright = bright;
		end
		if(sum(interface.sel.descripteurs==2))
			
				kurt = [kurt;mirgetdata(mirkurtosis(a))];
			
			structOut.kurt = kurt;
		end
		if(sum(interface.sel.descripteurs==3))
		
				b = mirgetdata(mirattacktime(a));
				try
				attackt = [attackt;b(1)];
				catch
					disp('attack')
					disp(t_obs)
					disp('chunk')
					disp(ii)
				end
			
			structOut.attackt = attackt;
		end
		if(sum(interface.sel.descripteurs==4))
			
				flat = [flat;mirgetdata(mirflatness(a))];
			
			structOut.flat = flat;
		end
		if(sum(interface.sel.descripteurs==5))
			
				centroid = [centroid;mirgetdata(mircentroid(a))];
			
			structOut.centroid = centroid;
		end
		if(sum(interface.sel.descripteurs==6))
			
				spread = [spread;mirgetdata(mirspread(a))];
			
			structOut.spread = spread;
		end
		if(sum(interface.sel.descripteurs==7))
			
				entrop = [entrop;mirgetdata(mirentropy(a))];
			
			structOut.entrop = entrop;
		end
		if(sum(interface.sel.descripteurs==8))
			
				b = mirgetdata(mirattackleap(a));
				attleap = [attleap;b(1)];
			
			structOut.attleap = attleap;
		end
		if(sum(interface.sel.descripteurs==9))
			
				skew = [skew;mirgetdata(mirskewness(a))];
			
			structOut.skew = skew;
		end
		if(sum(interface.sel.descripteurs==10))
				b = mirgetdata(mirattackslope(a));
				attslp = [attslp;b(1)];
			
			structOut.attslp = attslp;
		end
		if(sum(interface.sel.descripteurs==11))
			
				zerocross = [zerocross;mirgetdata(mirzerocross(a))];
			
			structOut.zerocross = zerocross;
		end
		if(sum(interface.sel.descripteurs==12))
			
				rolloff = [rolloff;mirgetdata(mirrolloff(a))];
			
			structOut.rolloff = rolloff;
		end
		if(sum(interface.sel.descripteurs==13))
			
				RMS = [RMS;mirgetdata(mirrms(a))];
			
			structOut.RMS = RMS;
		end
end



%(UNUSED)finalement on ne refait pas le match....on le fait � la composition de la
%structure..
function match = match_pieces(t_obs)
global structIn

piece = structIn(t_obs).piece;

switch piece
    case 1
        Nmat = midi2nmat('Xpiece_1.mid');
	case 2
        Nmat  = midi2nmat('Xpiece_2.mid');
	case 3
        Nmat  = midi2nmat('Xpiece_3.mid');
    case 4
        Nmat  = midi2nmat('Xpiece_4.mid');
end
Pmat = midi2nmat(structIn(t_obs).chemin_midi);
%On supprime le temps avant la premi�re note (les colones 1 et 6 de
%struct(i).midi sont les temps de tomb�es de chaque notes) 
debut = Pmat(1,1);
Pmat(:,1) = Pmat(:,1)-debut;
Pmat(:,6) = Pmat(:,6)-debut;
%matching
match = dynamicmatch(Pmat,Nmat,[num2str(structIn(t_obs).obs) ' '...
	structIn(t_obs).pianiste ' '...
	num2str(structIn(t_obs).piece) ' ' ...
	structIn(t_obs).nuance]);

function debfin = tableau_accords(t_obs)

global structIn
M = structIn(t_obs).match;
debfin = [];
mat = cell2mat([M.M(:,3),M.M(:,7),M.M(:,11)]);
for i=1:size(M.realGIp,1)
%chaque ligne du tableau realGIp repr�sente un accord on les mets
%successivement dans temp
	temp = M.realGIp(i,:);
	temp(temp==0)=[];
%on prends les temps de d�but et de fin de toutes les notes qui composent
%l'accord
	tempdeb= [];tempfin = [];
	
	for ii=1:length(temp)
		indtemp = find(mat(:,1)==temp(ii));
		tempdeb = [tempdeb;cell2mat(M.M(indtemp ,7))];
		tempfin = [tempfin;cell2mat(M.M(indtemp, 11))];
	end
	
	debfin = [debfin;min(tempdeb),min(tempdeb)+max(tempfin)];
end

% --- Executes on button press in dataMatch.
function dataMatch_Callback(hObject, eventdata, handles)
global interface


fig = figure;
set(fig,'MenuBar','none','NumberTitle','off');
liste	= findobj('Tag','listbox1');
temp	= get(liste,'String');
no		= get(liste,'Value');
selection = temp(no);
selection = strrep(selection,' ','');
selection = strsplit(selection{1},{' ','-'});
set(fig,'Name',[selection{1},' ',...
selection{2},' ',...
'pi�ce ',num2str(interface.sel.piece),' ',...
selection{3},' ']);
set(fig,'Position',[1 1 1150 620]);

table = uitable;
set(table,'Position',[20 20 1110 600],...
	'FontSize',12,...
	'data',interface.m.M,...
	'cellSelectionCallback',@cell_selected,...
	'RearrangeableColumns','on',...
	'ColumnName',{'Status','Notated index','Performance Index',...
	'Notated note #','Performance note #','Notated onset','Performance onset',...
	'Notated Velocity','Performance Velocity','Notated duration',...
	'Performance duration'});

function cell_selected(hObject, eventdata, handles)

lightUp(eventdata.Indices(1));

function remplir_tableau
global interface

%% the timer callback function definition
function plotMarker(obj, eventdata, player, figHandle, plotdata)           

% check if sound is playing, then only plot new marker
if strcmp(player.Running, 'on')
    
    % get the handle of current marker and delete the marker
    hMarker = findobj(figHandle, 'Color', 'y');
    delete(hMarker);
    
    % get the currently playing sample
    x = player.CurrentSample/player.SampleRate;
    
    % plot the new marker
    plot(repmat(x, size(plotdata)), plotdata, 'y');

end
