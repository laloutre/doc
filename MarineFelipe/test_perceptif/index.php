<html>
  <head>
    <meta charset="UTF-8">
    <title>Test perceptif</title>
     <style>
      body { background-color: #000000; }
      canvas { background-color: #333333; }
    </style>
  </head>
  <body>
  </body>
</html>


<body onload="load();">
<?php

  foreach (glob("../wav/*.wav") as $filename) {
      $php_array[] = "$filename" ;
  }
  $js_array = json_encode($php_array);
  $fp = fopen('js_array.json', 'w');
  fwrite($fp, $js_array);
  fclose($fp);
?>
  <div align="center">
    <canvas id="game-canvas" width="512" height="384"></canvas>
  </div>
  <script src="vendor/pixi.js"></script>
  <script src="vendor/jquery-1.11.3.min.js"></script>
  <script src="vendor/howler.min.js"></script>
  <script src="js/test_perceptif.js"></script>
</body>

