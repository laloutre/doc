


function load() { 
  var loader = PIXI.loader
      .add('sheet',"img/sprites.json")
      .load(function (loader, resources) {
        sprites = new PIXI.Sprite(resources.sheet.texture);
        
        // initKeyBoard();
        main();
    });
};



function main(){
stage = new PIXI.Container();
var sound;
var reponses;
var ordre;

renderer = PIXI.autoDetectRenderer(
    600,
    384,
    {view:document.getElementById("game-canvas")}
  );

 nom = window.prompt("Entrez votre prénom :","ici !");

play_button = new PIXI.Sprite.fromFrame('play');
play_button.position = new PIXI.Point(100,0);
play_button.scale = new PIXI.Point(0.1,0.1);
play_button.interactive =true;
play_button.play_lock =0;
play_button.on('mousedown', play);
stage.addChild(play_button);

stop_button = new PIXI.Sprite.fromFrame('stop');
stop_button.position = new PIXI.Point(100+play_button.width,0);
stop_button.scale = new PIXI.Point(0.1,0.1);
stop_button.interactive =true;
stop_button.play_lock =0;
stop_button.on('mousedown', stop);
stage.addChild(stop_button);

pause_button = new PIXI.Sprite.fromFrame('pause');
pause_button.position = new PIXI.Point(100+2*play_button.width,0);
pause_button.scale = new PIXI.Point(0.1,0.1);
pause_button.interactive =true;
pause_button.play_lock =0;
pause_button.on('mousedown', pause);
stage.addChild(pause_button);


answer1_button = new PIXI.Sprite.fromFrame('blueButton');
answer1_button.position = new PIXI.Point(0,play_button.height);
answer1_button.scale = new PIXI.Point(0.5,0.5);
answer1_button.interactive =true;
answer1_button.play_lock =0;
answer1_button.on('mousedown', answer);
answer1_button.texte = new PIXI.Text('Avec poids',{font : '80px Arial', fill : 0xff1010, align : 'center'});
answer1_button.texte.position = new PIXI.Point(answer1_button.width/2-50,answer1_button.height/2);
answer1_button.addChild(answer1_button.texte);
stage.addChild(answer1_button);

answer2_button = new PIXI.Sprite.fromFrame('blueButton');
answer2_button.position = new PIXI.Point(answer1_button.width,play_button.height);
answer2_button.scale = new PIXI.Point(0.5,0.5);
answer2_button.interactive =true;
answer2_button.play_lock =0;
answer2_button.on('mousedown', answer);
answer2_button.texte = new PIXI.Text('Sans poids',{font : '80px Arial', fill : 0xff1010, align : 'center'});
answer2_button.texte.position = new PIXI.Point(answer1_button.width/2-50,answer1_button.height/2);
answer2_button.addChild(answer2_button.texte);
stage.addChild(answer2_button);

save_button = new PIXI.Sprite.fromFrame('blueButton');
save_button.position = new PIXI.Point(answer1_button.width*0.60,2*play_button.height);
save_button.scale = new PIXI.Point(0.5,0.5);
save_button.interactive =true;
save_button.play_lock =0;
save_button.on('mousedown', saveToText);
save_button.texte = new PIXI.Text('sauver/quitter',{font : '80px Arial', fill : 0xff1010, align : 'center'});
save_button.texte.position = new PIXI.Point(answer1_button.width/2-70,answer1_button.height/2);
save_button.addChild(save_button.texte);
stage.addChild(save_button);

generate();
getSound();

refresh();
}

var json = (function () {
    var json = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': 'js_array.json',
        'dataType': "json",
        'success': function (data) {
            json = data;
        }
    });
    return json;
})(); 

var files = [];

function generate(){

var tab = [];
var random;
var i=0;
  
  do
  {

    random = Math.floor(Math.random() * json.length);
    
    var temp = json[random].split("/");
    temp = temp[2].split("_");
    num = temp[0]
    temp = temp[4].split(".wav");
    poids = temp[0];
    
    files[i] = {
      'poids':poids,
      'fichier':json.splice(random,1)[0]
    }
    i+=1;
  }while(json.length >= 1);
  console.log(files)
}

index_tab = 0;
function getSound(){
  sound = new Howl({
    urls: [files[index_tab].fichier, files[index_tab].fichier],
    buffer:true,
    onend:function(){play_button.play_lock=0;}
    });  
  console.log(files[index_tab].fichier);
}

function refresh(){
renderer.render(stage);  
}

function play(){
  if(this.play_lock == 0){
    this.play_lock =1;
    sound.play();
  }
}

function stop(){
    play_button.play_lock =0;
    sound.stop();
}

function pause(){
    play_button.play_lock =0;
    sound.pause();
}

function answer(){

  if((this.texte.text=='Avec poids' && parseInt(files[index_tab].poids)==1) || 
    (this.texte.text=='Sans poids' && parseInt(files[index_tab].poids)==0)){
    console.log('good')
    if(index_tab<files.length-1){
      stop();
      files[index_tab].reponse = 1;
      index_tab +=1;
      getSound();
    }
    else{
      stop();
      saveToText();
      // set reponses
      // exit
    }

  }else{
    console.log('niet')
    if(index_tab<files.length-1){
      stop();
      files[index_tab].reponse = 0;
      index_tab +=1;
      getSound();
    }
    else{
      stop();
      files[index_tab].reponse = 0;
      saveToText();
      // exit
    }
  }
}//ennnnnd
    

function saveToText(){
  $.ajax({
              type : 'POST',
              url : 'php/writeFile.php',
              data: {'prenom':nom,
                      'questionnaire':JSON.stringify(files)},
              success : function (d) {
              }
          });
  alert('Merci vous pouvez quitter la page !');
}
