DIMS = ('1_2');
% DIMS = ('2_3');
% DIMS = ('1_3');
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%figure de PCA
figs = [];
figs = [figs , figure(1)];
hold on;
%%on veut les 3 premi�res dimensions sur le graphe
for i = 1 : length(PCA.score(:,1))
			%%%%%%%%%%%mains
	if(data(i,:).main{1}=='E') %mains ensembles
			marker = 'd';
	elseif(data(i,:).main{1}=='D') %Main Droite
			marker = 's';
	elseif(data(i,:).main{1}=='G') %Main gauche
		marker = 'o';
    end
        %%%%%%%%%%%intensit�
        if(strcmp(data(i,:).nuance,'ff')==1)
            size = 16;
        elseif(strcmp(data(i,:).nuance,'f')==1)
            size = 14;
        elseif(strcmp(data(i,:).nuance,'mf')==1)
            size = 12;
        elseif(strcmp(data(i,:).nuance,'p')==1)
            size = 10;
        end
            
    
			%%%%%%%%%%%pedale
	if(data(i,:).pedale==1)
		pedalColor = 'g'; 
    else
        pedalColor = 'r';
    end
    markerFaceColor = pedalColor;
			%%%%%%%%%%%poid
	if(data(i,:).poids)
		markerFaceColor = pedalColor;
	else markerFaceColor = 'none';
	end
	
	plot3(PCA.score(i,1),PCA.score(i,2),PCA.score(i,3),...
				'Marker',marker,...
				'MarkerSize',size,...
				'MarkerFaceColor',markerFaceColor,...
				'Color',pedalColor,...
				'lineWidth',2);
			
% 	temp = data(i,:).name;
% 	text(PCA.score(i,1)+0.03,PCA.score(i,2)+0.03,PCA.score(i,3)+0.03,...
% 			temp);
end
			

% %Cercle des variables
figs = [figs , figure(2)];
hold on;

%coefforth = inv(diag(std(double(out.p2))))*out.coeff;
biplot(PCA.coeff(:,1:3),'Positive',true,'varlabels',PCA.VarNames);
grid;
xlabel(strcat('Dim1 : ',num2str(PCA.explained(1)),'% expliqu�e'));
ylabel(strcat('Dim2 : ',num2str(PCA.explained(2)),'% expliqu�e'));
zlabel(strcat('Dim3 : ',num2str(PCA.explained(3)),'% expliqu�e'));
if(strcmp(DIMS,'1_2')==1)
angle_vue = [0 90];% DImentions 1 et 2

elseif(strcmp(DIMS,'1_3')==1)
angle_vue = [-180 0];% DImentions 1 et 3

elseif(strcmp(DIMS,'2_3')==1)
angle_vue = [90 0];% DImentions 2 et 3
end
view( angle_vue);
%on trace un cercle
ang=0:0.01:2*pi; 
r=1;
xp=r*cos(ang);
yp=r*sin(ang);
if(strcmp(DIMS,'1_2'))
    plot(0+xp,0+yp);
    clear r;
    set(gca,'XLim',[-1.5 1.5]);
    set(gca,'YLim',[-1 1]);
else 
    plot3(0+xp,zeros(length(yp),1)',0+yp);
    set(gca,'XLim',[-1.5 1.5]);
    set(gca,'ZLim',[-1 1]);
end




figs = [figs , figure(3)]
hold on;
title('Variance expliqu�e');
pareto(PCA.explained);
xlabel('Composantes principales');
ylabel('Variance expliqu�e(%)');

figs = [figs , figure(5)];
	hold on;
boxplot(zscore(double(dataPCA)),'orientation','horizontal',...
'labels',PCA.VarNames);
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% %%%%print
% 
for i =1 : length(figs)
	set(figs(i),'units','normalized','outerposition',[0 0 1 1]);
	
	set(figs(i),'PaperSize',[20 20],'PaperPosition', [0 0 10 10]);
	print(figs(i),'-dpdf',sprintf('../figures/Piece%d',i));
end

figure(1)


disp('j''aime la gym')