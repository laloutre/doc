function data = matching()
WAV_DIR         = '../wavARP/';
MIDI_DIR        = '../midiARP/';

wav_file = dir([WAV_DIR,'*.wav']);

arpege = {};
arpege{1} = midi2nmat('../midi/arpeges/Arpege_-3.mid');
arpege{2} = midi2nmat('../midi/arpeges/Arpege_-2.mid');
arpege{3} = midi2nmat('../midi/arpeges/Arpege_-1.mid');
arpege{4} = midi2nmat('../midi/arpeges/Arpege_0.mid');
arpege{5} = midi2nmat('../midi/arpeges/Arpege_1.mid');
arpege{6}= midi2nmat('../midi/arpeges/Arpege_2.mid');
arpege{7} = midi2nmat('../midi/arpeges/Arpege_3.mid');

data = rmfield(wav_file,'bytes');
data = rmfield(data,'isdir');
data = rmfield(data,'datenum');

for i=1 : length(wav_file)
    data(i).name = strsplit(data(i).name,'.');
    data(i).name = data(i).name{1};
    
    temp =strsplit(data(i).name,'_');
    
    data(i).rec = str2double(temp{1});
    data(i).octave = temp{2};
    data(i).nuance = temp{3};
    data(i).poids = str2double(temp{4});
    
    data(i).nmat = midi2nmat([MIDI_DIR,num2str(data(i).rec),'.mid']);
    
        data(i).name
        if(size(data(i).nmat,1)>0)
        data(i).nmat(:,6) = data(i).nmat(:,1); %il y a une erreur soit dans dynamicmatch soit dans midi2nmat...(Ken Schutte's function)
        data(i).match = dynamicmatch(data(i).nmat,...
            arpege{str2double(data(i).octave)+4});
        data(i).match.M
    
        data(i).match.title = [' poid : ', num2str(data(i).poids),' octave : ',data(i).octave];
        end
end


data = struct2dataset(data);
data(strcmp(data.octave,'-3')==1,:) = [];
button = questdlg('Lancer le slicing maintenant ?','On fait quoi ?','oui','non','oui');
if(strcmp(button,'oui')==1)data = script_2_slicingARP(data);
end
