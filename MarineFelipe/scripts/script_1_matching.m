function data = matching()
WAV_DIR         = '../wav/';
MIDI_DIR        = '../midi/';

wav_file = dir([WAV_DIR,'*.wav']);

partGauche = midi2nmat('../midi/Partition/notaG.mid');
partDroite = midi2nmat('../midi/Partition/notaD.mid');
partDeuxMains = midi2nmat('../midi/Partition/notaDG.mid');

data = rmfield(wav_file,'bytes');
data = rmfield(data,'isdir');
data = rmfield(data,'datenum');

for i=1 : length(wav_file)
    data(i).name = strsplit(data(i).name,'.');
    data(i).name = data(i).name{1};
    
    temp =strsplit(data(i).name,'_');
    
    data(i).rec = str2double(temp{1});
    data(i).main = temp{2};
    data(i).nuance = temp{3};
    data(i).poids = str2double(temp{4});
    data(i).pedale = str2double(temp{5});
    
    data(i).nmat = midi2nmat([MIDI_DIR,num2str(data(i).rec),'.mid']);
    
    if(data(i).main == 'G')
        data(i).name
        data(i).nmat(:,6) = data(i).nmat(:,1); %il y a une erreur soit dans dynamicmatch soit dans midi2nmat...(Ken Schutte's function)
        data(i).match = dynamicmatch(data(i).nmat,partGauche);
        data(i).match.M
    
    elseif(data(i).main == 'D')
        data(i).name
        data(i).nmat(:,6) = data(i).nmat(:,1);%il y a une erreur soit dans dynamicmatch soit dans midi2nmat...(Ken Schutte's function)
        data(i).match = dynamicmatch(data(i).nmat,partDroite);
        data(i).match.M
    
    elseif(data(i).main == 'E')
        data(i).name
        data(i).nmat(:,6) = data(i).nmat(:,1);%il y a une erreur soit dans dynamicmatch soit dans midi2nmat...(Ken Schutte's function)
        data(i).match = dynamicmatch(data(i).nmat,partDeuxMains);
        data(i).match.M
    end
    
    data(i).match.title = [' main : ', data(i).main,' poid : ', num2str(data(i).poids), ' p�dale : ', num2str(data(i).pedale)];
    
end


data = struct2dataset(data);

button = questdlg('Lancer le slicing maintenant ?','On fait quoi ?','oui','non','oui');
if(strcmp(button,'oui')==1)data = script_2_slicing(data);
end
