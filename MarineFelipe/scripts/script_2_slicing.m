function data = slicing(data)
warning('off','all');
data.vecteur ={};
for i=1:length(data)

    
    match = [cell2mat(data(i,:).match.M(:,2)),cell2mat(data(i,:).match.M(:,7))];
    vecteur = [match(1,1),match(1,2)];
    counter = 1;
    
    for j=2:length(match)
        if(match(j-1,1)==match(j,1) && match(j-1,2)>match(j,2))
            vecteur(counter,2) = match(j-1,2);
        elseif(match(j-1,1)<match(j,1))
            counter = counter+1;
            vecteur(counter,2) = match(j,2);
        end
    end
data(i,:).vecteur = {vecteur};
end

end

% Y' a plus qu' a faire un joli mirsegment(['../wav/',test.name{1},'.wav'],test.vecteur{1}(:,2))

