
warning('off','all');
f = fopen('ordre.txt','a');

data.descripteurs = {};
for i =1 : length(data)
    
    tempDesc = dataset();
    obs = data(i,:);   %Pourquoi les prendre dans cet ordre ? c' est melant
    sr = mirsegment(['../wavARP/',obs.name{1},'.wav'],obs.vecteur{1}(:,2))
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     title(obs.name{1});
% 	set(figure(1),'Name',obs.name{1});
%     print('figures','-dpsc','-append');
% 	BookMark = sprintf('[/Title (%s) /Page %d /OUT pdfmark',obs.name{1},i);
%     fprintf(f,BookMark);
%     ghostscript('-q -dBATCH -dNOPAUSE -sDEVICE=pdfwrite -sOutputFile=figures.pdf  figures.ps ordre.txt');
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    sr = mirgetdata(sr);
    for j = 1:size(sr,2)
        segment = sr(:,j);
        segment=segment(~isnan(segment));
        segmAudio = miraudio(segment);
        tempDesc.RMS(j,1)    = mirgetdata(mirrms(segmAudio))
        tempDesc.brightness(j,1)    = mirgetdata(mirbrightness(segmAudio))
        
        tempDesc.attackT(j,1)       = mean(mirgetdata(mirattacktime(segmAudio)))
        tempDesc.attackS(j,1)       = mean(mirgetdata(mirattackslope(segmAudio)))
        tempDesc.attackL(j,1)       = mean(mirgetdata(mirattackleap(segmAudio)))
        
        tempDesc.kurtosis(j,1)      = mirgetdata(mirkurtosis(segmAudio))
        tempDesc.spread(j,1)        = mean(mirgetdata(mirspread(segmAudio)))
        tempDesc.skewness(j,1)      = mean(mirgetdata(mirskewness(segmAudio)))
        tempDesc.flatness(j,1)      = mean(mirgetdata(mirflatness(segmAudio)))
        tempDesc.entropy(j,1)       = mean(mirgetdata(mirentropy(segmAudio)))
    end
    data(i,:).descripteurs = {tempDesc};
    close all;
    clc;
end