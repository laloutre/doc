
for j =1:length(data.descripteurs{1}.Properties.VarNames)
eval(['data.',data.descripteurs{1}.Properties.VarNames{j},'={}'])
end
for i=1 : length(data)

obs = data(i,:).descripteurs{1};

for j =1:length(obs.Properties.VarNames)
eval(['data(i,:).',obs.Properties.VarNames{j},'={',num2str(mean(double(obs(:,j)))),'}'])
end

end

dataPCA = data;
dataPCA.name = [];
dataPCA.date = [];
dataPCA.rec = [];
dataPCA.nuance = [];
dataPCA.poids = [];
dataPCA.nmat = [];
dataPCA.match = [];
dataPCA.vecteur = [];
dataPCA.descripteurs = [];
dataPCA.octave= [];


for i = 1 :length(dataPCA.Properties.VarNames)
eval(['dataPCA.',dataPCA.Properties.VarNames{i},'=cell2mat(dataPCA.',dataPCA.Properties.VarNames{i},')']);
end

[PCA.coeff,PCA.score,PCA.latent,PCA.tsquared,PCA.explained,PCA.mu] = pca(zscore(double(dataPCA)));
% [coeff,score,latent,tsquared,explained,mu] = pca(double(dataPCA));

PCA.VarNames = dataPCA.Properties.VarNames;

clear i j obs;