\section{Méthodologie}

\subsection{Enregistrement des nuances de timbre}

Pour la collecte des données (paramètres de contrôle et captation sonore), de courtes pièces originales ont été spécialement composées. La consigne qui a été donnée aux compositeurs était de créer des pièces dont la structure ou les mélodies permettraient l'expression de chacune des cinq nuances de timbre étudiées en ne favorisant cependant aucune d'entre elles en particulier. En quelque sorte, les pièces devaient être timbralement neutres. Ces pièces font quelques mesures chacune et les durées ne dépassent pas la vingtaine de secondes, les métriques sont différentes et plusieurs aspects techniques de l'interprétation pianistique y sont inclus.

Ces pièces ont été jouées par quatre pianistes (une femme et trois hommes) de niveau professionnel. Nous les identifierons par leurs initiales dans la suite de l'article (BB, RB, PL, FP). Il est  à noter que l'un d'entre eux (FP) possède l'oreille absolue.

Les quatre interprètes ont reçu  à l'avance les partitions et les cinq nuances de timbre à exprimer pour pouvoir les répéter. Les répétitions et les enregistrements ont eu lieu au studio du BRAMS (Laboratoire international de recherche sur le Cerveau, la Musique et le Son) sur le piano à queue Bösendorfer Imperial (modèle~290) équipé d'un système d'enregistrement des paramètres de contrôle (mouvement des touches, des pédales et des marteaux). Ces données indexées en fonction du temps sont enregistrées dans des fichiers au format \textit{.boe}. 

Lors des sessions d'enregistrement, les pianistes ont reçu la consigne d'interpréter les pièces en leur donnant tour à tour une couleur timbrale parmi les cinq suivantes : brillant, sombre, rond, sec, velouté. La série complète a été jouée trois fois : deux fois dans un ordre de pièces et de timbres choisis par le participant et une fois dans un ordre aléatoire imposé.
Le nombre total d'exécutions de ces courtes pièces s'élève ainsi à 240 (4 pièces $\times$ 5 nuances de timbre $\times$ 4 interprètes $\times$ 3 sessions = 240 enregistrements).

	\begin{figure}[H]
	\begin{center}
	\includegraphics[width=\columnwidth]{partitions_3pieces.pdf}
	\caption{Trois pièces composées pour l'étude sur le timbre au piano}
	\label{piece1}
	\end{center}
	\end{figure}
			
Pour la captation sonore elle-même, deux microphones cardioïdes DPA 4011-TL ont été utilisés pour les prises stéréophoniques proches. Ils ont été disposés en XY à peu près à 1~m du côté ouvert du couvercle du piano, à mi-chemin entre le clavier et le fond de la caisse et à une hauteur de 1,20 m. Les microphones pointaient vers la table d'harmonie du piano.

Deux micros DPA 4006 omnidirectionnels, aussi disposés en XY ont été postés à environ 50~cm derrière les microphones cardioïdes à une hauteur de 1,40~m et pointant au même endroit pour une prise d'ambiance et pour ajouter la réverbération naturelle de la salle dans les enregistrements.

Les signaux des microphones ont été dirigés dans un préamplificateur Millenia HV-2D puis enregistrés à 44.1~kHz, avec une résolution dynamique de 24 bits. Les quatre pistes ont ensuite été mixées en stéréo avec le logiciel Logic~9\copyR ~et un fichier \textit{wav} à été créé pour chacune des 240 interprétations.
		
\subsection{Segmentation}

De façon à pouvoir extraire des descripteurs acoustiques relatifs à l'attaque et à l'évolution temporelle de chaque note ou de chaque groupe de notes jouées simultanément (accords), une segmentation des captations sonores a dû être effectuée dans un premier temps. La segmentation des notes jouées sur un piano est un problème complexe du fait de la nature polyphonique de l'instrument. Plusieurs notes peuvent en effet être jouées simultanément et les notes peuvent également se superposer dans le temps, par l'usage d'une articulation liée ou par l'usage des pédales. La segmentation basée sur l'analyse du contenu spectro-temporel d'une interprétation au piano n'étant pas toujours très robuste, nous exploitons les données de contrôle qui ont été captées par le piano enregistreur (Bösendorfer CEUS) simultanément aux données acoustiques.

Pour effectuer la segmentation de nos captations sonores, nous avons donc dans un premier temps converti les fichiers \textit{.boe} en fichiers MIDI, qui contiennent des informations sur le numéro des touches jouées, sur l'intensité avec laquelle elles sont pressées ainsi que deux informations temporelles relatives au moment initial de l'enfoncement de la touche et à son relâchement (note On / note Off). Dans un deuxième temps, nous avons extrait des fichiers MIDI des partitions encodées dans le logiciel de notation musicale (Sibellius\copyR). Ces fichiers MIDI correspondent à une restitution mécanique et régulière de la partition. Finalement, nous avons utilisé un algorithme permettant d’aligner les données MIDI des prestations enregistrées sur les données MIDI de la partition (l'algorithme de \textit{score matching} développé par \citet{Large1993b}). De cette manière, chacune des notes de la partition a pu être localisée dans le temps dans chacun des extraits sonores. 

L'alignement MIDI n'ayant pas bien fonctionné pour la pièce 3 (vraisemblablement à cause du trille rapide qu'elle contient), il a été décidé d'écarter les données correspondant à cette pièce. 

L'ensemble de données analysées contient finalement 180 fichiers audio (pièces 1, 2 et 4) segmentés, évènement par évènement.	
	
\subsection{Calcul des descripteurs acoustiques}

11 descripteurs acoustiques ont été sélectionnés parmi les fonctions proposées par la MirToolbox \citep{Lartillot2013} dans l'environnement Matlab\copyR. Ces descripteurs sont : 
le centre de gravité spectral (\textit{centroid}), l'étalement spectral (\textit{spread}), l'asymétrie de la distribution spectrale (\textit{skewness}), le \textit{kurtosis} (ou coefficient d'aplatissement de Pearson), le ratio entre la moyenne géométrique et la moyenne arithmétique de la distribution (\textit{flatness}), l'\textit{entropie}, la \textit{brillance} (reliée à la quantité d'énergie du signal contenue au-dessus de 1500~Hz), le \textit{rolloff} spectral (la fréquence en dessous de laquelle 85\% de l'énergie spectrale est contenue), le \textit{zerocross} (le nombre de fois que le signal change de signe), le \textit{temps d'attaque} de chaque évènement, la pente moyenne de l'attaque (\textit{attack slope}) et la différence d'amplitude entre le début et la fin de chaque attaque (\textit{attack leap}).
	
Ces descripteurs sont calculés pour chacun des événements des 180 extraits. 
Soulignons qu'une interface dédiée a été élaborée (dans Matlab) pour permettre la visualisation des résultats de l'alignement de partition  ainsi que de ceux de la segmentation, ou encore pour calculer des descripteurs spécifiques sur des pièces choisies par l'utilisateur. Les retours fournis par cette interface sont des ensembles de données (\textit{datasets}) sur lesquels plusieurs fonctions statistiques de Matlab peuvent être appliquées sans modifications préalables.
	
Les valeurs moyennes des onze descripteurs acoustiques relatifs à chaque évènement sont calculées sur la totalité de la durée des pièces. Chaque pièce est donc représentée par 11 valeurs et les analyses statistiques présentées dans la suite de l'article s'appuient sur ces valeurs moyennes.
		