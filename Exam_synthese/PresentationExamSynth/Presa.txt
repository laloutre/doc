Bonjour à vous.

Aujourd'hui je vous présente les réalisations pratiques que sur lesquelles j'ai travaillé au cours de ces deux années de doctorat.
En fait il y a quatres parties que je voudrais développer, 

Premièrement je voudrais vous faire écouter en guise d'introduction les stimuli sur lesquels nous travaillons et vous parler des conditions de leur enregistrement. 
Puis nous verrons comment nous pratiquons les analyses acoustiques avec une des toolbox matlab dont nous disposons : la mirtoolbox.
Dans un troisième temps je vous présenterais comment nous pouvons analyser les micros-variations de timing avec ableton live et un outil développé sous matlab d'ailleurs en collaboration avec Sylvain.
Puis finalement je vous montrerais l'interface de matching max/msp développée pour évaluer la pertience des résultats.


Donc premièrement les stimuli : en fait comme vous avez pu le lire dans mon travail écrit, un des objectifs de ma recherche de doctorat est de mieux comprendre ce qui différencie au niveau acoustique une interprétation "sombre": 

(FAIRE ÉCOUTER UNE INTERPRÉTATION)

d'une interprétation "brillante" : 

(FAIRE ÉCOUTER UNE AUTRE INTERPÉTATION)

Ces stimuli sont issus de la banque de 240 sons sur laquelle je travaille et que nous avons pu enregistrer en collaboration avec Michel Bernays. Dans le détail il s'agit de cinq oeuvres originales interprétées par cinq pianistes (qui ?) de niveau professionnel et soulignant cinq nuances timbrales différentes(sec brillant sombre velouté rond). Nous avons tout mis en oeuvre pour que les conditions d'enregistrement soient rigoureusement les mêmes dans chaque cas.

Alors comment évaluer ce qui différencie ces stimulis d'un point de vue acoustique ? et bien c'est le deuxième point, nous utilisons pour cela des programmes codés sous matlab, dont la MIRtoolbox. 

(DÉMO)

En parallèle il est possible de travailler sur les variations de tempo grâce à la procédure que nous avons mis au point avec Sylvain : 

(DEMO)

Parler de PCA ?

Finalement nous avons pu collaborer sur la création d’un test de perception avec Michel Bernays, un test donc pour évaluer la pertinence des résultats. Il se présente sous la forme d’un patch max/Msp que je vais vous présenter maintenant.

(DEMO)

