\subsection{Timbre et acoustique}\label{methodoacoustique}
 Les méthodologies utilisées pour observer les aspects acoustiques du timbre sont relativement récentes et reposent en partie sur des outils informatiques. Grâce à ces derniers, les chercheurs calculent un nombre plus ou moins important de \textit{descripteurs acoustiques} qui peuvent évoluer dans le temps au fil de la pièce ou du stimulus considéré ou le représenter au complet. Les descripteurs calculés sont choisis arbitrairement par le chercheur, mais certains d'entre eux reviennent dans de nombreuses études en raison de leur importance déjà démontrée, aussi les \textit{sets} de descripteurs utilisés tendent à se «standardiser». Une description exhaustive des outils permettant ce type d'analyse acoustique sera faite au \ref{mirtoolbox}.
 Une fois cette étape accomplie, il faut trouver les corrélations qui peuvent exister entre ces descripteurs acoustiques du son et leurs contreparties perceptives. Une façon courante de calculer le coefficient de corrélation entre deux variables $x$ et $y$ est la suivante : \\
 $r_{p} = \frac{\sigma_{xy}}{\sigma_{x}\sigma_{y}}$\\
 où $\sigma_{xy}$ désigne la covariance entre les variables \textit{x} et \textit{y}, et $\sigma_{x}$, $\sigma{y}$ leur écart type.\\
 Le coefficient peut alors prendre une valeur entre -1 et 1, 1 indiquant une corrélation, -1 une «anti»— corrélation et 0 une absence de corrélation entre la variable perceptive et la variable acoustique considérées. Il faut préciser ici qu'une corrélation entre deux variables n'indique pas nécessairement une relation de causalité.
 
  \begin{figure}[H]
  \begin{center}
  \includegraphics[scale=0.75]{experienceacoustique.png}
  \caption{Résumé d'une approche typique pour l'étude des corrélats acoustiques du timbre.}
  \label{experienceacoustique}
  \end{center}
  \end{figure}
 Dans les expériences que nous décrirons ci-dessous les chercheurs ont appliqué cette démarche méthodologique pour comprendre quels paramètres rentraient en jeu lors de notre identification du timbre. Dans la plupart des cas, les chercheurs ont voulu modifier un unique paramètre d'un ou plusieurs sons d'instruments réels (ou synthétisés) et \textbf{tester la probabilité de leur reconnaissance}. Bien que ces expériences ne soient pas les plus récentes, elles présentent un intérêt certain puisqu'elles sont encore aujourd'hui une grande partie de notre connaissance du timbre. À noter encore que beaucoup de ces expériences sont résumées dans \citet{Hajda1997}.
 \subsubsection{Influence différentes parties de l'enveloppe}
  Les études de \citet{Saldanha1964}, \citet{clark1963preliminary}, \citet{Wedin1972}, \citet{Elliott1975} et \citet{Berger1964}, visent à isoler les \textbf{différentes parties de l'enveloppe des sons et à comprendre leurs influences respectives dans la perception du timbre}. 
 Par enveloppe, nous entendons ici la valeur $x_{rms}$ en fonction du temps définie comme suit :\\
 $x_{rms} = \sqrt{\frac{1}{n}(x_1^2+x_2^2+...x_n^2+)}$\\ 
 Cette courbe prend souvent sous la forme de quatre segments que l'on nomme attaque délai Maintient chute (ou ADSR pour Attack Delay Sustain Release).
 Pourtant, peu de chercheurs décrivent alors précisément ces segments, il ne semble pas y avoir de consensus sur ce point. Dans certaines recherches, la durée de l'attaque est fixée arbitrairement, ce qui n'est pas satisfaisant. Pourtant certains résultats concluent que :
  \begin{itemize}
  \item \textit{Si on enlève l'attaque, il est plus difficile d'identifier les instruments (sauf pour les instruments à commande continue avec vibrato).}
  \item \textit{L'absence du segment decay ne semble pas gêner l'identification.}
  \end{itemize}
 
 \subsubsection{Influence du contexte mélodique}
 \citet{campbell1978contribution}, pour tester \textbf{l'influence du contexte mélodique sur le timbre}, introduisent le paramètre legato comme une variable [2]. Ils testent l'identification d'instruments avec et sans legato lors de l'attaque et du sustain et concluent que sa présence permet une meilleure identification. Grâce à l'introduction du legato on se rapproche un peu plus de l'identification en contexte musical. Malgré tout, il reste un problème de définition, car dans cette étude les chercheurs considèrent que l'attaque est d'une durée fixe et égale pour tous les sons.
 \citet{Kendall1986} s'intéresse au rôle de l'attaque et du legato lors de la phase sustain du son, mais contrairement à Campbell il définit l'attaque et le legato en fonction du signal audio. Il propose une série de tests de matching à l'issue desquels il conclue que la détection se fait aussi bien sur les sons altérés que non altérés. Aurait-on surestimé le rôle des transitoires dans la reconnaissance du timbre ?
 McAdams pense qu'en supprimant l'attaque d'un son on ne fait que la remplacer par une autre attaque «synthétique». En effet, si on définit l'attaque d'un son comme le «début» de ce son, alors la supprimer revient à la remplacer par autre chose. McAdams propose donc de masquer l'attaque au lieu de la supprimer.
 Hajda pense pour sa part que du fait qu'aucune définition précise des constituants de l'enveloppe temporelle, on ne peut que difficilement comprendre leur rôle dans l'identification timbrale. 
 
 \citet{beauchamp1982synthesis} souligne le fait que l'enveloppe temporelle (RMS) et le centre de gravité spectral présentent une grande similitude durant les états de sustain. Aussi il introduit un nouveau descripteur acoustique qu'il appelle ACT (Amplitude/Centroïd Trajectory) qui rend compte de l'évolution simultanément de l'enveloppe temporelle et de la trajectoire du centre de gravité. Pour \citet{Hajda1997} c'est un descripteur pertinent pour le timbre encore inexploré puisqu'il rend compte de caractéristiques fréquentielles et temporelles en même temps. Hajda émet à ce sujet les hypothèses suivantes :
    
  \begin{itemize}
  \item \textit{L'identification n'est pas affectée si la première région de l'ACT (transition entre attaque et sustain) est préservée bien que les caractères spectraux puissent être changés.}
  \item \textit{Dans les études où les caractéristiques spectrales sont conservées, mais où le début de l'ACT est retiré, il est possible que l'identification soit plus difficile. Non pas à cause de la modification spectrale, mais plutôt par le fait qu'on ait retiré la transition attaque/sustain.}
  \end{itemize}  
 \subsubsection{Influence des caractéristiques spectrales et temporelles }
 \textbf{L'influence des caractéristiques spectrales et temporelles sur le timbre} a aussi été observée en testant des sons dont l'enveloppe temporelle et/ou spectrale était remplacée par celle d'autres sons.
 \citet{Strong1967} pensent que l'enveloppe spectrale est plus importante pour l'identification que l'enveloppe temporelle lorsque les caractéristiques spectrales de l'instrument sont uniques, ou plutôt non présentes avec d'autres instruments. Lorsque les caractéristiques spectrales ne sont pas uniques, l'enveloppe temporelle joue un rôle perceptif égal à celui de l'enveloppe spectrale. L'analyse multidimensionnelle permet de souligner les points communs de plusieurs stimuli, l'unicité d'un stimulus ne peut donc pas être représentée dans cette analyse puisqu'un seul instrument est concerné. Mais plus récemment, \citet{winsberg1989quasi} ont développé un modèle de MDS qui permet à un stimulus d'avoir sa propre dimension en combinaison des dimensions partagées avec les autres stimuli.
 \citet{plomp1976aspects,plomp1970timbre}, utilise des sons de synthèse continus inspirés de 9 instruments acoustiques et trouve un espace de représentation des distances perceptives en trois dimensions. \citet{Wedin1972} utilisent des sons stationnaires avec et sans les transitoires et les mêmes instruments que Plomp et trouvent aussi trois dimensions en rapport avec la distribution d'énergie dans le sustain :
  \begin{itemize}
  \item \textit{Haute énergie globale.}
  \item \textit{Énergie décroissante dans les hautes fréquences.}
  \item \textit{Intensité faible de la fondamentale et augmentation des autres composantes.}
  \end{itemize}
 \citet{wessel1973psychoacoustics} et Hajda utilisent les mêmes instruments que Plomp et trouvent seulement deux dimensions :
  \begin{itemize}
  \item Les caractéristiques hautes et basses fréquences des attaques.
  \item Le centre de gravité spectral.
  \end{itemize}
 Hajda pense que ces trois expériences ne sont pas cohérentes, car les stimuli sont tout de même différents. Cependant, il existe une forte corrélation entre certains aspects de la perception et certaines caractéristiques spectrales.
 \citet{Grey1977a}, de son côté, compare des sons originaux avec des sons issus de synthèse et d'autres issus d'une réduction des sons de synthèse. Il trouve que les distances perceptives sont réparties en fonction de trois paramètres physiques qui corroborent les résultats de Wessel :
  \begin{itemize}
  \item Le centre de gravité spectral. 
  \item La synchronicité des transitoires dans les hautes fréquences.
  \item La présence d'énergie dans les hautes fréquences lors de l'attaque.
  \end{itemize}
 \citet{Krumhansl1989}, testent la reconnaissance de 21 timbres synthétisés par modulation de fréquence (Yamaha TX802). Quatorze de ces timbres sont des imitations d'instruments réels, et sept sont des hybrides. Ici encore la répartition perceptive semble liée à trois paramètres :
  \begin{itemize}
  \item La rapidité de l'attaque (sépare les sons impulsifs/continus). 
  \item Le centre de gravité spectral.
  \item Le flux spectral
  \end{itemize}
 Enfin,\citet{Kendall1991} s'intéressent aux sons de divers instruments à vent. Chaque stimulus est composé d'un enregistrement de deux instruments simultanés enregistrés à l'unisson, en mélodie, séparés d'une tierce majeure ou harmonisés. Les enregistrements d'unisson et quelques-uns des enregistrements harmonisés ont été soumis à des tests de similarité, d'identification, de différenciation sémantique et d'estimation de magnitude verbale. Les résultats montrent qu'il existe deux dimensions :
  \begin{itemize}
  \item Nasal opposée à non nasal 
  \item Riche opposé brillant
  \end{itemize}
 Une troisième dimension a été postulée : simple opposé à compliqué, mais le nombre restreint de stimuli ne permet pas de certifier cette dernière. De plus, une analyse acoustique a révélé que :
  \begin{itemize}
  \item La quantité d'énergie dans les plus hautes fréquences lors du sustain séparait les sons de hautbois nasal des autres (une version de
  la distorsion harmonique).
  \item Le degré de variance temporelle, aide à séparer les saxophones alto en trémolo des autres.
  \end{itemize}
  
 Plus récemment, cette approche est utilisée dans les travaux de doctorat de \citetapos{Barthet2008} sur le rôle du timbre dans l'interprétation de clarinette. Ses résultats démontrent entre autres qu'«\textit{un clarinettiste (expert) reproduit de manière systématique certaines variations de timbre (Temps d'Attaque, Centre de Gravité Spectral, Rapport [ndr des harmoniques] Pair/Impair) lors de répétitions d'interprétations produites selon la même intention musicale.}» et que «\textit{La nature des variations de timbre change lorsque l'intention expressive de l'interprète change (par ex. brillance plus élevée, Temps d'Attaque plus long, etc.).}»
 De plus, le chercheur propose des expériences dans lesquelles des auditeurs doivent juger du naturel de sons dont il a manipulé le timbre (la brillance) et ses résultats prouvent que les sujets ont tendance à juger plus proches (perceptivement) des stimuli pour lesquels la modification de brillance appliquée est la même. Cela démontre qu'il serait en fait possible, en manipulant des descripteurs tels que la brillance, de rendre les sons de synthèses de clarinette plus proches de la réalité, plus expressifs. 
 Pour parvenir à ces résultats, Barthet a lui-même développé des outils informatiques qu'il a regroupés sous le nom de Matimbre Toolbox, nous détaillerons les possibilités qu'offrent ce genre d'outils au \ref{mirtoolbox}.
  
 Ainsi, les études mentionnées fournissent un panel intéressant de méthodologies et de résultats issus de diverses disciplines pour étudier les aspects perceptifs et acoustiques des nuances d'expressivité. Évidemment, une étude plus globale nécessitera sans doute le recours à des méthodes conçues pour observer d'autres aspects de ces couleurs. Nous pensons par exemple que le recours à l'éventail technique et théorique pensé pour observer les implications temporelles de telles nuances peut s'avérer d'une grande utilité pour une meilleure compréhension.