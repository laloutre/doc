 Dans cette sous-partie nous passerons en revue certaines méthodologies expérimentales déjà appliquées à l'étude du timbre. Nous discuterons ensuite de leurs points forts et de leurs points faibles pour finalement proposer notre propre approche méthodologique.
 
 
\subsection{Timbre et perception}\label{TimbreetPerception}
  
 Dans les années soixante-dix, un certain nombre d'études sur le timbre \citetapos{Grey1977a, Grey1978,Wessel1979} avaient déjà pour objectif de mettre la lumière sur les paramètres physiques du signal qui jouent un rôle dans notre capacité à percevoir différents instruments, ce que \citetapos{Marozeau2004} nomme : le \textit{timbre identité}. Ces travaux ont introduit des méthodologies qui sont encore utilisées aujourd'hui, notamment l'utilisation du positionnement multidimensionnel (\textit{Multidimensionnal Data Scaling}) pour représenter des matrices de confusions dans un espace Euclidien \citetapos{Bernays2013c,Guillemain2010}. Cette méthodologie appliquée à l'étude du timbre se présente souvent comme suit : un expérimentateur cherche à représenter les distances perceptives qui séparent différents timbres d'instruments. Il dispose pour ce faire de plusieurs stimuli audio d'instruments qu'il devra d'abord égaliser en intensité et en hauteur (en effet la définition ANSI du timbre comme vu au \ref{defANSI} est : ce qui n'est ni la hauteur ni l'intensité d'un son et qui nous permet de le distinguer d'un autre). Cette étape réalisée, il présente les stimuli par paires aux participants et leur demande de juger de leur \textit{dissimilarité} sur une échelle qu'il aura lui même fixé (1 à 10 par exemple), les résultats se présentent sous la forme d'un tableau à double entrée aussi appelé \textit{matrice de dissimilarités}. L'algorithme de MDS (\textit{Multidimensional Data Scaling}) prends alors cette donnée en entrée et va en proposer plusieurs représentations Euclidiennes au chercheur (par exemple une représentation en deux dimensions, une en trois et une en quatre etc) ainsi qu'un coefficient de satisfaction pour chacune des représentations possibles. Le chercheur pourra déterminer la représentation qui lui convient le mieux en fonction du coefficient de satisfaction et de sa propre perception (les représentations en deux ou trois dimensions sont plus facilement compréhensibles que celles en quatre). Mais bien que l'algorithme ait maintenant représenté les distances perceptives entre chaque stimuli, il ne fournit aucun indice sur les raisons physiques de cet éloignement (voir Figure \ref{mdsgrey}) : c'est au chercheur de les déduire. Dès lors, il est par exemple possible de calculer un descripteur acoustique (ex : le \textit{centre de gravité spectral}) pour chacun des stimuli et de "classer" les stimuli par ordre de croissant pour ce descripteur : si cet ordre est identique à celui dans lequel se répartissent les stimuli sur l'un des axes de la représentation, il est très probable que cet axe soit \textit{corrélé} au descripteur acoustique calculé (ici le \textit{centre de gravité spectral}). D'autres fonctions permettrons d'ailleurs au chercheur de calculer le degré de \textit{corrélation} entre les valeurs des descripteurs et la répartition sur un axe donné.
 
 \begin{figure}[h]
 \label{mdsgrey}
 \begin{center}
 \includegraphics{grey77mds.png}
 \caption{Exemple de représentation de 35 matrices de similarités généré par Multidimensional Data Scaling \citetaposp{Grey1977a}{p. 1272}. À cette étape le chercheur ne sait pas encore à quels paramètres physiques correspondent les dimensions I, II et III.}
 \end{center}
 \end{figure}
 Cependant, ces études sur la perception, bien que novatrices en terme de méthodologie, ont pour objet d'étude le timbre inter-instrument, ou \textit{timbre-identité} \citetapos{Marozeau2004}. Il n'existait alors --à notre connaissance-- aucune étude sur le \textit{timbre-qualité}. 
 
 Il existe un lien fort entre intensité et timbre au piano \citetapos{Parncutt2002} néanmoins, cette assertion est aussi à l'origine du scepticisme à propos des nuances timbrales au piano, Parncutt s'exprime d'ailleurs assez clairement à ce sujet : 
 \begin{quote}
 [...] we may assert that the spectral and temporal envelopes of an isolated piano tone cannot be changed independently of its SPL (\textit{ndr:} Sound Pressure Level); hence, timbre cannot be changed independently of loudness.\citetp{Parncutt2002}{289}
 \end{quote}
 Malgrès tout, \citet{Bellemare2005} ont voulu étudier la perception qu'on les pianistes des nuances timbrales de leur instrument. Leur enquête, reposant sur une méthode de \textit{verbalisation libre}, a montré que les pianistes de niveau professionnel utilisent plus d'une centaine de descripteurs verbaux pour parler des nuances de timbre qu'ils sont capables de produire (voir \textit{figure} \ref{DescMad}). 
 
 \begin{figure}[h]
 \begin{center}
 \includegraphics{DescMad.png}
 \caption{Atlas sémantique du timbre au piano: arrangement qualitatif des descripteurs verbaux par évaluation du degré de synonymie \citetapos{Bellemare2005a}}
 \label{DescMad}
 \end{center}
 \end{figure}
 
 Dans ce cas, comment concilier une analyse scientifique \citetapos{Parncutt2002}, prouvant que sur une note unique aucune nuance timbrale n'est possible (seule l'intensité peut varier) et l'expérience des pianistes, qui entendent et sont capables de décrire plus d'une centaine de nuances timbrales \citetapos{Bellemare2005} ? Nous soutenons qu'il existe un juste milieu entre ces deux points de vue. D'une part, nous pensons qu'il faut étudier les variations de timbre non pas sur une note isolée, mais plutôt en situation de jeu, car si nous reconnaissons que seule l'intensité peut modifier le son d'une note isolée, nous pensons aussi que l'articulation \citetapos{Repp1997}, le doigté \citetapos{Parncutt2002} ou le synchronisme \citetapos{Goebl2005}, jouent un rôle certain dans notre perception des nuances de timbre. D'autre part, si nous constatons que les pianistes utilisent plus d'une centaine de descripteurs pour parler de la palette des timbres disponibles au piano, nous pensons que certains des termes ont une proximité sémantique telle qu'il est possible de les regrouper sous une même famille, réduisant ainsi le nombre de descripteurs.
  \begin{figure}[H] 
  \begin{center}
  \includegraphics{4dMike.png}
  \caption{Répartition des quatorze descripteurs utilisés dans l'étude de Michel Bernays \citetaposp{Bernays2011a}{p. 13} sur les deux premières dimensions (à gauche) puis sur en fonction des dimensions trois et quatre.}
  \label{4dMike}
  \end{center}
  \end{figure}
 C'est d'ailleurs cette dernière hypothèse qu'explore Michel Bernays dans son travail sur la proximité sémantique des adjectifs descripteurs du timbre au piano \citetapos{Bernays2011a}. Ce travail présente un double intérêt puisqu'il se base en partie sur les méthodologies présentées dans les paragraphes précédant et qu'il porte de surcroit sur l'objet de la présente étude : le piano. Le chercheur sélectionne d'abord les vingt-neuf descripteurs cités plus d'une fois dans l'étude de \citet{Bellemare2005} puis, par les relations synonymiques qu'entretiennent certains d'entre eux, il réduit ce corpus à quatorze descripteurs : \textbf{Brassy, Bright, Clear, Dark, Distant, Dry, Full-bodied, Harsh, Metallic, Muddled, Round, Shimmering, Soft, Velvety}.
 Bernays présente ensuite ces descripteurs par paires --dans un questionnaire-- à dix-sept pianistes. Les participants évaluent leur propre familiarité avec chacun des descripteurs sur une échelle d’un à cinq puis doivent juger --sur le même type d'échelle-- de la proximité sémantique de chacun des descripteurs par rapport aux autres (91 paires au total). La compilation de ces évaluations de proximité est ensuite transformée en matrices de dissimilarité et entrée dans un algorithme de positionnement multidimensionnel. Le résultat de ce dernier montre que seulement quatre dimensions (voir Figure \ref{4dMike}) sont nécessaires pour représenter l'espace originel de quatorze dimensions (avec une corrélation entre les deux représentations de 92.4\%).
  \begin{figure}[H] 
  \begin{center}
  \includegraphics{ClusterMike.png}
  \caption{Dendrogramme du clustering hiérarchique des dissimilarités sémantiques entre les quatorze descripteurs du timbre au piano\citetaposp{Bernays2011a}{p .303} sur les deux premières dimensions obtenues par analyse MDS}
  \label{ClusterMike}
  \end{center}
  \end{figure}
 Bernays complète cette analyse en appliquant sur les distances euclidiennes qui séparent les descripteurs un algorithme de \textit{clusterisation}, dont les résultats (sous forme de dendrogramme) sont présentés à la Figure \ref{ClusterMike}. L'application d'un tel algorithme est très intéressante dans ce cas de figure, car elle permet non seulement de regrouper les descripteurs en \textit{"familles"}, mais aussi de compléter les résultats du positionnement multidimensionnel en regroupant lesdites familles sur les cartes de distances perceptives (voir Figure \ref{DescMike}).
  \begin{figure}[H] 
  \begin{center}
  \includegraphics{DimensionsMichel.png}
  \caption{Répartition des quatorze descripteurs utilisés dans l'étude de Michel Bernays \citetaposp{Bernays2013c}{p. 202} sur les deux premières dimensions obtenues par analyse MDS. Les Adjectifs proches selon l'analyse en cluster (voir Figure \ref{ClusterMike}) sont encadrés en rouge.}
  \label{DescMike}
  \end{center}
  \end{figure}
 Cependant, études perceptives vont souvent de pair avec des études acoustiques. En effet, comme nous l'avons vu dans cette sous-section, s’il est possible de représenter les distances perceptives entre différents stimuli, la suite logique de cette étape est de déterminer quels facteurs acoustiques en sont la cause.
 