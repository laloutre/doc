
\subsection{Tempo et timing}\label{methodotimings}
   
 Carl Emil Seashore posait ainsi les bases pour l'étude de l'interprétation expressive dès les années 1930. L'expressivité musicale était alors définie comme une déviation de hauteur, de timing, de tempo, de timbre ou de dynamique par rapport à ce que pourrait être un «rendu métronomique, de hauteur parfaite, de timbre pur et de rythme rigide» de la partition. Dès lors, l'expressivité pouvait être mesurée en tant que déviation temporelle par rapport au tempo parfait (celui proposé par le compositeur)
 
  \begin{quote}
  The unlimited resources for vocal and instrumental art lie in artistic deviation from the pure, the exact, the perfect, the rigid the even and the precise. This deviation from exact is, on the whole, the medium for the creation of the beautiful --for the conveying of emotion. That is the secret of the plasticity of art. The exact is cold, restricted, and unemotional; and, however beautiful, in itself soon palls upon us.\citetaposp{Seashore1925}{538} 
 \end{quote} 
 
 Cette définition a été abandonnée assez tôt, car il est apparu évident qu'un auditeur peut apprécier une pièce sans en connaître la partition. Il lui est alors impossible de se faire une idée des déviations de la partition que l'interprète produit par son jeu, mais il peut tout de même ressentir l'expressivité de l'interprète.\\
 
 La littérature au sujet des variations de timing et de tempo dans l'interprétation expressive est très riche. Rink (cité par \citetapos{Mcpherson2013}) avance d'ailleurs que cela pourrait être dû au fait qu'il est plus simple de mesurer et de modéliser rigoureusement ces paramètres face à d'autres comme les nuances timbrales, ou les mouvements (de posture) du musicien qui restent plus réfractaires à une quantification précise. Il n'en reste pas moins vrai qu'il existe, comme le montrent les expériences de \citet{Clynes1995} sur les microstructures musicales, une pulsation rythmique spécifique et nettement reconnaissable propre à certains compositeurs --\citet{Repp1989} émet tout de même un doute sur la généralisation du concept. Ces expériences, aussi reproduites par \citet{Repp1989}, prouvent (s'il le fallait encore) l'importance des micro déviations de timing.
 
 Il existe dans l'étude des timings et du tempo plusieurs approches méthodologiques, la première historiquement est celle (comme nous en parlions au \ref{deftimings}) qui découle des travaux de Seashore et propose d'étudier le phénomène comme une déviation temporelle de la partition. Dans ce type d'études, le chercheur relève par différents moyens (qui se sont d'ailleurs sophistiqués avec le temps) les temps auxquels l'interprète joue chaque note (ou accord si elles sont simultanées) et compare ces relevés avec les temps théoriques auxquels elles auraient du apparaitre. Les temps théoriques sont déduits à l'aide des durées des notes sur la partition. Ultérieurement, il est possible de déduire les tempi de la manière suivante : puisque le dénominateur de la signature rythmique sur la partition précise combien de \textit{beats} sont présents dans une mesure et que les relevés temporels renseignent sur les durées entre deux \textit{beats} (en secondes par exemple), il est possible de faire le calcul suivant pour avoir le tempo en \textit{beats} par minutes : \\
 
 $BpM(t) = 60/BpM(t)-BpM(t-1)$
 
 \begin{figure}[h]
  \includegraphics{taxman.png}
  \caption{Graphe de tempo pour «Taxman» du groupe The Beatles (issu de \citetaposp{Robertson2012a}{p. 1}).}
  \label{Taxman}
 \end{figure}
 
 Ces calculs, effectués pour une pièce au complet, affichent souvent, comme à la Figure \ref{Taxman}, une forme en \textit{arche} associée à chaque phrase musicale permettant aussi la comparaison plus aisée entre différents interprètes/interprétations. Il est à noter qu'il existe aussi deux autres manières de représenter ces courbes de tempo : soit comme des déviations par rapport à un tempo donné, auquel cas il faut soustraire ce tempo à toutes les valeurs de tempi obtenus dans les calculs précédents (Time-Shift représentation), soit comme une déviation temporelle par rapport au temps théorique de la pièce (Time-Map représentation). Les trois formes de courbes sont représentées à la Figure \ref{time} (issue de \citet{Honing2001}) dans laquelle trois tempi sont affichés \textit{f,g} et \textit{h}, représentant respectivement un tempo constant \textit{f}, un tempo qui monte puis redescends \textit{g} et un changement de tempo \textit{h}. Les lignes b et c sont leurs représentations en tant que déviation par rapport à un tempo constant et leur écart par rapport au temps théorique occurrence des évènements.
 
 \begin{figure}[h]
 \begin{center}
  \includegraphics{Time.png}
  \caption{Trois manières de représenter les déviations temporelles (issues de \citetp{Honing2001}{52)}. La première ligne représente des fonctions de tempo f, g et h, la seconde leur représentation en tant que fonction de décalage temporel et la troisième en tant que carte temporelle.}
  \label{time}
  \end{center}
 \end{figure}
 
 Cependant, les défauts de ces courbes soulignés par \citet{Desain1993} sont que ces représentations des tempi ne permettent pas de distinguer un changement de tempo, d'une note retardée ou avancée (pour l'accentuer par exemple) d'un changement de tempo local (comme vu au \ref{deftimings}) de plus, la représentation en courbe donne à penser que la variation du tempo est un phénomène continu alors qu'il s'agit en fait de calculs discrets. Aussi, le chercheur propose une démarche reposant sur l'observation de la déviation non plus par rapport à un tempo global, mais par rapport au tempo de plus petites unités structurelles \citetapos{Desain1991a}. Une autre manière de remédier à ce problème est de représenter non plus les bpm, mais les Inter Onset Intervals (IOI) qui sont une mesure en temps de la distance entre deux évènements.
 
 Pourtant, nous pensons, comme \citet{Repp1998a} ou plus récemment comme \citet{Robertson2012a}, que la représentation des bpm ou des IOI permet tout de même de produire des graphiques présentant certes une imprécision, mais qui restent valides dans le cas de \textit{comparaisons}, et qui montrent des patterns généraux assez similaires entre musiciens. De plus, ils présentent l'avantage d'être  assez simples à mettre en place pour un grand nombre d'échantillons sonores.
 
 Enfin, une dernière approche mise en place pour visualiser les variations expressives de tempo est celle de \citet{Goebl2003} qui est souvent appelée \textit{worm} (Voir Figure \ref{worm}) qui représente non seulement le tempo en bpm mais aussi l'intensité perçue en sones. Dans ce type de graphe, l'évolution temporelle est représentée par l'augmentation croissante des points de relevé. L'intensité et le tempo pouvant tous les deux avoir des valeurs plus faibles à l'instant \textit{t+1} qu'à l'instant \textit{t}, la représentation graphique présente souvent l'aspect d'un ver : d'où le nom de \textit{worm}. Ce genre de représentation est intéressante puisqu'elle est la seule qui présente les deux variables dynamique et tempo sur un même graphe et que ces deux aspects de l'interprétation expressive peuvent être liés.
 
 \begin{figure}[h]
 \begin{center}
  \includegraphics{worm.png}
  \caption{Représentation en \textit{ver} des mesures 1 à 21 de l'étude Op. 10, No. 3 de Chopin (issue de \citetaposp{Goebl2003}{p. 75})}
  \label{worm}
  \end{center}
 \end{figure}
 
 À noter que, lorsque dans un article de 1999, \citet{Repp1999a} propose à quatre juges d'évaluer la qualité esthétique de 100 interprétations de l'ouverture de l'Étude en mi majeur de Chopin, les résultats montrent un phénomène intéressant. En effet, les variables timing et dynamique ne semblent expliquer que 9 à 18\% de la variance des notes données par les juges. Repp explique alors qu'il \textit{"[...]suspecte que l'aspect esthétique le plus important dans ces performances soit la qualité insaisissable souvent appelée «toucher»» } et que cela \textit{"[...]reste l'aspect le moins compris des compétences pianistiques, mais sans doute le plus important.»}
 Justement, il convient à ce moment de mentionner les travaux de \citet{Bernays2013c} qui visent à combler ce manque. Comme nous l'avons vu au \ref{TimbreetPerception}, sa première étude sur la perception du timbre par les pianistes \citetapos{Bernays2011a} montre (voir Figure\ref{ClusterMike}) que cinq familles d'adjectifs peuvent être nettement distinguées. Aussi, le chercheur choisit un adjectif parmi chacune de ces familles (Brillant, sombre,rond, velouté, sec) et tente d'en établir un portrait gestuel. Encore une fois, la méthodologie est ici très intéressante : le chercheur demande en premier lieu à trois compositeurs de créer quatre pièces miniatures (quatre mesures chacune) ne favorisant aucune des cinq nuances timbrales mentionnées plus haut. Puis quatre pianistes professionnels participent à l'expérience en jouant chacune des quatre pièces composées avec, tour à tour, chacun des cinq caractères timbraux. 
 Les musiciens jouent sur un piano de concert équipé de capteurs de gestes (\textit{Bösendorfer Ceus}) capables d'enregistrer les nuances dans le toucher des interprètes avec une précision supérieure à celle utilisée habituellement dans ce genre d'expérience (8 bits pour le Bösendorfer contre 7 bits pour le MIDI). Bien sûr, afin d'exploiter ces données temporelles brutes, Bernays a créé un outil informatique (la BösenToolbox) lui permettant d'extraire des caractéristiques gestuelles et de composer un «portrait» pour chacune des cinq nuances de timbre (voir Figure \ref{kiviat}).
  
   
 \begin{figure}[h] 
 \begin{center}
  \includegraphics{kiviat.png}
  \caption{Représentation des profils gestuels pour les cinq nuances de timbre étudiées dans les travaux de \citetaposp{Bernays2011a}{p. 213}}
  \label{kiviat}
  \end{center}
 \end{figure}