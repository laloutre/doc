\select@language {french}
\select@language {french}
\contentsline {chapter}{\MakeUppercase {Contents}}{ii}{section*.1}
\contentsline {chapter}{\numberline {\MakeUppercase {Chapitre} 1:}\MakeUppercase {Introduction}}{1}{chapter.1}
\contentsline {chapter}{\numberline {\MakeUppercase {Chapitre} 2:}\MakeUppercase {Le timbre et l'analyse de l'interpr\IeC {\'e}tation expressive}}{4}{chapter.2}
\contentsline {section}{\numberline {2.1}D\IeC {\'e}finitions}{4}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Timbre}{4}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Tempo et Timing}{7}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}M\IeC {\'e}thodologies}{9}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Timbre et perception}{9}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Timbre et acoustique}{15}{subsection.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.1}Influence diff\IeC {\'e}rentes parties de l'enveloppe}{18}{subsubsection.2.2.2.1}
\contentsline {subsubsection}{\numberline {2.2.2.2}Influence du contexte m\IeC {\'e}lodique}{18}{subsubsection.2.2.2.2}
\contentsline {subsubsection}{\numberline {2.2.2.3}Influence des caract\IeC {\'e}ristiques spectrales et temporelles }{20}{subsubsection.2.2.2.3}
\contentsline {subsection}{\numberline {2.2.3}Tempo et timing}{22}{subsection.2.2.3}
\contentsline {section}{\numberline {2.3}R\IeC {\'e}sultats et discussion}{29}{section.2.3}
\contentsline {chapter}{\numberline {\MakeUppercase {Chapitre} 3:}\MakeUppercase {M\IeC {\'e}thodologie et outils}}{31}{chapter.3}
\contentsline {section}{\numberline {3.1}M\IeC {\'e}thodologie}{31}{section.3.1}
\contentsline {section}{\numberline {3.2}Application de la m\IeC {\'e}thodologie et outils}{33}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Stimuli}{33}{subsection.3.2.1}
\contentsline {subsection}{\numberline {3.2.2}MirToolbox, Matimbre, Timbre Toolbox et Matlab}{36}{subsection.3.2.2}
\contentsline {subsection}{\numberline {3.2.3}Analyse du tempo}{37}{subsection.3.2.3}
\contentsline {subsection}{\numberline {3.2.4}Tests de perception}{39}{subsection.3.2.4}
\contentsline {chapter}{\numberline {\MakeUppercase {Chapitre} 4:}\MakeUppercase {\IeC {\'E}volution de la facture pianistique et cons\IeC {\'e}quences sur le timbre}}{41}{chapter.4}
\contentsline {section}{\numberline {4.1}Introduction}{41}{section.4.1}
\contentsline {section}{\numberline {4.2}\IeC {\'E}volution de la manufacture et \IeC {\'e}volution du timbre}{41}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}\IeC {\'E}volution de la manufacture du piano-forte}{42}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}\IeC {\'E}volution du timbre}{44}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}En quoi l'\IeC {\'e}volution des sonorit\IeC {\'e}s peut modifier l\IeC {\textquoteright }exp\IeC {\'e}rience musicale}{47}{subsection.4.2.3}
\contentsline {section}{\numberline {4.3}Pourquoi la technique pianistique serait-elle indissociable de la manufacture de son \IeC {\'e}poque}{48}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Du geste pianistique comme geste excitateur...}{48}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}...Au geste pianistique en tant que geste modificateur}{50}{subsection.4.3.2}
\contentsline {subsection}{\numberline {4.3.3}\IeC {\'E}volution de l\IeC {\textquoteright }exp\IeC {\'e}rience musicale de l'interpr\IeC {\`e}te : le cas du jeu perl\IeC {\'e}}{52}{subsection.4.3.3}
\contentsline {section}{\numberline {4.4}Travail th\IeC {\'e}orique sur l'apport de l'acoustique}{54}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}Caract\IeC {\'e}ristiques temporelles}{54}{subsection.4.4.1}
\contentsline {subsection}{\numberline {4.4.2}Caract\IeC {\'e}ristiques timbrales}{57}{subsection.4.4.2}
\contentsline {subsection}{\numberline {4.4.3}Points de frappe des marteaux}{58}{subsection.4.4.3}
\contentsline {section}{\numberline {4.5}Travail pratique et exp\IeC {\'e}rimental}{61}{section.4.5}
\contentsline {subsection}{\numberline {4.5.1}Enregistrement de pianos}{61}{subsection.4.5.1}
\contentsline {subsubsection}{\numberline {4.5.1.1}Enregistrements acoustiques}{61}{subsubsection.4.5.1.1}
\contentsline {subsubsection}{\numberline {4.5.1.2}Pianoteq$^{\unhbox \voidb@x \hbox {\relax \fontsize {8}{9.5}\selectfont {\copyright }}}$}{62}{subsubsection.4.5.1.2}
\contentsline {subsection}{\numberline {4.5.2}Segmentation}{63}{subsection.4.5.2}
\contentsline {subsection}{\numberline {4.5.3}Mesures de timbre}{63}{subsection.4.5.3}
\contentsline {subsubsection}{\numberline {4.5.3.1}Spectres}{63}{subsubsection.4.5.3.1}
\contentsline {subsubsection}{\numberline {4.5.3.2}Inharmonicit\IeC {\'e}}{64}{subsubsection.4.5.3.2}
\contentsline {subsubsection}{\numberline {4.5.3.3}Points de frappe}{65}{subsubsection.4.5.3.3}
\contentsline {subsubsection}{\numberline {4.5.3.4}Brillance}{65}{subsubsection.4.5.3.4}
\contentsline {subsection}{\numberline {4.5.4}Mesures temporelles}{65}{subsection.4.5.4}
\contentsline {subsubsection}{\numberline {4.5.4.1}Attaque}{65}{subsubsection.4.5.4.1}
\contentsline {subsubsection}{\numberline {4.5.4.2}Onsets}{65}{subsubsection.4.5.4.2}
\contentsline {subsubsection}{\numberline {4.5.4.3}Release}{66}{subsubsection.4.5.4.3}
\contentsline {subsection}{\numberline {4.5.5}R\IeC {\'e}sultats et discussion}{66}{subsection.4.5.5}
\contentsline {section}{\numberline {4.6}Conclusion et perspectives}{68}{section.4.6}
\contentsline {chapter}{\numberline {\MakeUppercase {Chapitre} 5:}\MakeUppercase {Conclusion}}{70}{chapter.5}
\vspace \bigskipamount 
\contentsline {chapter}{\MakeUppercase {Bibliographie}}{71}{section*.20}
\contentsline {chapter}{\numberline {\MakeUppercase {Annexe} I:}\MakeUppercase {Annexes}}{iv}{appendix.A}
