
% Some calculations upon Perception Results

w=who;

% Matrix of results per timbre (col.) per participant (row)
for i=1:length(w), eval(['results_per_timbre(' num2str(i) ',:) = ' w{i} '.ScoresPerTimbre;']); end

% Idem per pianist
for i=1:length(w), eval(['res_per_pianist(' num2str(i) ',:) = sum(' w{i} '.PianistScores,2)'';']); end

% Idem per piece
for i=1:length(w), eval(['results_per_piece(' num2str(i) ',:) = sum(' w{i} '.PieceScores,2)'';']); end



% Mean success rate per pianist over all participants
rate_per_pianist = sum(results_per_pianist)/(60*6/3);

% Idem per piece
rate_per_piece = sum(results_per_piece)/(60*6/4);

% Idem per timbre
rate_per_timbre = sum(results_per_timbre)/(60*6/5);