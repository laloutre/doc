function ok = conversion( rep)
% usage 
%conversion(repertoire);
% programme qui permet de convertir en matrices 
%de dissimilarité tous les les fichiers .txt d'un repertoire
%Les fichiers doivent respecter la forme :

% 0, 3 1 0.755906;
% 1, 2 1 0.188976;
% 2, 1 3 0.80315;
% 3, 1 2 0.314961;
% 4, 3 2 0.748031;
% 5, 2 3 0.220472;
% chanson1;

rep = strcat(pwd,'/*.txt');
fichiers = dir(rep);
for ind = 1:size(fichiers)
    conversions(fichiers(ind).name);
end


function  mat  = conversions( fichier )



FILE = fopen(fichier);
mat = [];
matrice = [];
delim = [';' '"' '.' '/'];
pat = '/\w*\.';
test_names = '';



nom = strtok(fichier,'.');
sujet = struct();

tline = fgetl(FILE);
while ischar(tline)
    [L,ok] = str2num(tline);
    if(ok)
        mat = [mat;L];
        
    else 
        mat(:,1) = [];
        test_nb = 0;
        
        tline = regexp(tline, pat, 'match');
        tline = strtok(tline,delim);
        test_names = fieldnames(sujet);  
               
        test_nb = sum(strcmp(tline,test_names));
        if(test_nb) 
            tline = strcat(tline,num2str(test_nb)); 
        end
        sujet = setfield(sujet,char(tline),mat);
        mat(:,:) = [] ;    
    
    end
    tline = fgetl(FILE);
end
fclose(FILE);




noms_matrices = fieldnames(sujet);

for i = 1 :  length(noms_matrices) 
    
   %sujet.(char(noms_matrices(i))) = organise( sujet.(char(noms_matrices(i))));
end
assignin('base',nom,sujet);



function rangement = organise( matrice)

taille = size(matrice);
taille = taille(1);
eq = strcat('n*(n-1) = ',num2str(taille));
taille_fin = max(double(solve(eq)));

rangement = [];
for ind = 1 : taille
    i = matrice(ind,1);
    j = matrice(ind,2);
    k = matrice(ind,3);
    rangement(i,j) = k;
end
return








